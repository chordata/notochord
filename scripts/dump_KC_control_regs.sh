#!/bin/bash
#
# This script will open all the gates from a Chordata HUB and
# dump the acel and gyro Control registers from the found KCeptors
# 
# It is adviced to plug just one KCeptor at the time to avoid confusion
# and address clashing  
set -x

AG_ADDR=0x6b

echo "CTRL_REG1_G"
i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR 0x10

echo "CTRL_REG2_G"
i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR 0x11

echo "CTRL_REG3_G"
i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR 0x12

echo "CTRL_REG4_G"
i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR 0x1e

echo "CTRL_REG5_XL"
i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR 0x1f

echo "CTRL_REG6_XL"
i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR 0x20

echo "CTRL_REG7_XL"
i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR 0x21

echo "CTRL_REG8"
i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR 0x22

echo "CTRL_REG9"
i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR 0x23

echo "CTRL_REG10"
i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR 0x24