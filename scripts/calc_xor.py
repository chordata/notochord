#!/usr/bin/python

import sys

#print 'Number of arguments:', len(sys.argv), 'arguments.'
#print 'Argument List:', str(sys.argv)

if len(sys.argv) > 1:
	val = sys.argv[1]
	
	print "[  XM  ] XOR 0x6b: {0} ( 0x{0:02X} )".format(int(val, 16) ^ 0x6b)
	print "[  G   ] XOR 0x1e: {0} ( 0x{0:02X} )".format(int(val, 16) ^ 0x1e)
else:
	print "give a value to have it XORed with 0x6b, 0x1d (LSM9DS1 adresses))"
	
