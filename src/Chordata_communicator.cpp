/**
 * @file Chordata_communicator.cpp
 * Definitions for the communicator namespace
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <tuple>

#include "fmt/core.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/null_sink.h"
#include "spdlog/sinks/ansicolor_sink.h"
#include "spdlog/sinks/daily_file_sink.h"

#include "Chordata_communicator.h"
#include "Chordata_error.h"

using namespace Chordata::Communicator;
using namespace std;
namespace spd = spdlog;

comm_fn_t Chordata::Communicator::output_handler,
		Chordata::Communicator::trace_handler,
		Chordata::Communicator::debug_handler,
		Chordata::Communicator::info_handler,
		Chordata::Communicator::warn_handler,
		Chordata::Communicator::err_handler;

transmit_fn_t Chordata::Communicator::transmit_handler;

Chordata::Timekeeper Chordata::Communicator::timekeeper;

long Chordata::Communicator::log_attemps = 0;

Comm_Substrate::Comm_Substrate(const Chordata::Configuration_Data& global_config){
	Chordata::Configuration_Data config = global_config;

	switch (global_config.comm.log_level){
		case 1: log_level = spd::level::debug; break;
		case 2: log_level = spd::level::trace; break;
		default:log_level = spd::level::info;
		}

	err_level = (global_config.comm.log_level == 0)?
		spd::level::err : spd::level::warn;	

	if ( 0 != create_dir( fmt::format("{}/{}", global_config.exe_path ,_CHORDATA_FILEOUT_DIR).c_str()))
		throw Chordata::System_Error(errno);
	
	auto push_redirect = [&config] (const auto source, auto& dest, auto level)
		{
		for (unsigned i =0,mask; i<=_Output_Redirect_keywords_n; ++i ){
			mask = 1 << i-1;
			switch (source & mask){
				case Chordata::NONE:
				break;
				case Chordata::STDOUT:
					dest.push_back(make_shared<spdlog::sinks::ansicolor_stdout_sink_mt>());
					// cout<< "stdout"<< endl;
				break;
				case Chordata::STDERR:
					dest.push_back(make_shared<spdlog::sinks::ansicolor_stderr_sink_mt>());
					// cout<< "STDERR"<< endl;
				break;
				case Chordata::FILE:
					dest.push_back(make_shared
									<spdlog::sinks::daily_file_sink_mt>
										(config.comm.filename, 0, 0)
									);
					// cout<< "FILE"<< endl;
				break;
				case Chordata::OSC:
					dest.push_back(make_shared<OscOut>(config));
					// cout<< "OSC"<< endl;
				break;
			}
		}
	};
	
	push_redirect(global_config.comm.transmit, transmit, log_level);

	
	Chordata::Output_Redirect log_redirect = global_config.comm.log;
	Chordata::Output_Redirect err_redirect = global_config.comm.error;

	push_redirect(log_redirect, log, log_level);
	push_redirect(err_redirect, error, err_level);

}

#define _PATTERN_FORMAT "[{{:>{}}}] {{}}"
#define _LOGGING_PATTERN "%^%l~%$%v"

using spdlevel = spdlog::level::level_enum;

#define TRANSMIT_OSC(S,Q) osc_sink->transmit(S,Q)/*; osc_sink->flush()*/
#define TRANSMIT_OTHERS(S,Q) logger.log(which, "{},{:d},{:f},{:f},{:f},{:f}", S, timekeeper.ellapsedMillis(), Q.w, Q.x, Q.y, Q.z);/*\
							logger.flush()*/

static std::shared_ptr<OscOut> osc_sink;

auto Chordata::Communicator::initializers::transmit_fn(spdlevel which, Comm_Substrate::redirects&& v, spdlevel global_level){
	static auto transmitRedirects = std::move(v);


	// for (auto& r:transmitRedirects){
	// 	if (auto o = dynamic_pointer_cast<OscOut>(r))
	// 		osc_sink = std::move(o);
	// }

	auto r = std::begin(transmitRedirects);
	while (r != std::end(transmitRedirects)) {
	    // Do some stuff
	    if (auto o = dynamic_pointer_cast<OscOut>(*r)){
	    	osc_sink = std::move(o);
	        r = transmitRedirects.erase(r);
	    }
	    else
	        ++r;
	}

	if (transmitRedirects.size() == 0)
		transmitRedirects.push_back(make_shared<spdlog::sinks::null_sink_mt>());
	
	static spdlog::logger logger("transmit_logger", transmitRedirects.begin(), transmitRedirects.end());
	logger.set_level(global_level);
	logger.set_pattern("%v");

	static auto pattern = fmt::format(_PATTERN_FORMAT,  _CHORDATA_PRINT_MILLIS_PADDING );

	comm_fn_t outAction = [ which, pattern=pattern.c_str() ](const std::string& s){ 
		logger.log(which, pattern, timekeeper.ellapsedMillis(), s);
		logger.flush();

	};

	transmit_fn_t transmitAction;

	if (osc_sink && transmitRedirects.size() > 0){
		transmitAction = [which](const std::string& s, const Quaternion& q){
			TRANSMIT_OSC(s,q); 	TRANSMIT_OTHERS(s,q); };
	} else if  (osc_sink){
		transmitAction = [](const std::string& s, const Quaternion& q){
			TRANSMIT_OSC(s,q); };
	} else {
		transmitAction = [which](const std::string& s, const Quaternion& q){
			TRANSMIT_OTHERS(s,q); };
	}

	return std::make_tuple(outAction,transmitAction);
}


comm_fn_t Chordata::Communicator::initializers::log_fn(spdlevel which, Comm_Substrate::redirects&& v, spdlevel global_level){
	static auto outRedirects= std::move(v);

	if (outRedirects.size() == 0)
		outRedirects.push_back(make_shared<spdlog::sinks::null_sink_mt>());
	
	static spdlog::logger logger("output_logger", outRedirects.begin(), outRedirects.end());
	logger.set_level(global_level);
	logger.set_pattern(_LOGGING_PATTERN);
	// logger.flush_on(spd::level::err);

	static auto pattern = fmt::format(_PATTERN_FORMAT,  _CHORDATA_PRINT_MILLIS_PADDING );

	comm_fn_t outAction = [ which, pattern=pattern.c_str() ](const std::string& s){ 
		logger.log(which, pattern, timekeeper.ellapsedMillis(), s); 
	};

	return outAction;
}

comm_fn_t Chordata::Communicator::initializers::error_fn(spdlevel which, Comm_Substrate::redirects&& v, spdlevel global_level){
	static auto errRedirects= std::move(v);
	
	if (errRedirects.size() == 0)
		errRedirects.push_back(make_shared<spdlog::sinks::null_sink_mt>());
	
	static spdlog::logger logger("error_logger", errRedirects.begin(), errRedirects.end());
	logger.set_level(global_level);
	logger.set_pattern(_LOGGING_PATTERN);
	logger.flush_on(spd::level::warn);

	static auto pattern = fmt::format(_PATTERN_FORMAT,  _CHORDATA_PRINT_MILLIS_PADDING );

	comm_fn_t outAction = [ which, pattern=pattern.c_str() ](const std::string& s){ 
		logger.log(which, pattern, timekeeper.ellapsedMillis(), s); 
	};

	return outAction;
}

void Chordata::Communicator::init_communicator(const Chordata::Configuration_Data& global_config){
	Chordata::Communicator::init_communicator(Chordata::Communicator::Comm_Substrate(global_config));	
}


void Chordata::Communicator::init_communicator(Comm_Substrate&& c){
	static bool done = false;
	if (!done){
		done = true;
	} else {
		warn("Only the first init_communicator() call has effect");
		return;
	}

	// size_t q_size = _CHORDATA_LOG_QUEUE_SIZE;
 //    spdlog::set_async_mode(q_size, spdlog::async_overflow_policy::block_retry,
 //                       nullptr,
 //                       std::chrono::seconds(_CHORDATA_LOG_QUEUE_SIZE));

    auto outputs = initializers::transmit_fn( spd::level::info, std::move(c.transmit), c.log_level );

	Chordata::Communicator::output_handler = std::get<0>(outputs);

	Chordata::Communicator::transmit_handler = std::get<1>(outputs);

	Chordata::Communicator::trace_handler =
			initializers::log_fn( spd::level::trace, std::move(c.log), c.log_level );

	Chordata::Communicator::debug_handler =
			initializers::log_fn( spd::level::debug, std::move(c.log), c.log_level );

	Chordata::Communicator::info_handler =
			initializers::log_fn( spd::level::info, std::move(c.log), c.log_level );

	Chordata::Communicator::warn_handler =
			initializers::error_fn( spd::level::warn, std::move(c.error), c.err_level );

	Chordata::Communicator::err_handler =
			initializers::error_fn( spd::level::err, std::move(c.error), c.err_level );



	if (Chordata::Communicator::log_attemps > 0){
		Chordata::Communicator::warn("{} loging attemps made, before the communicator functions were initialized", 
			Chordata::Communicator::log_attemps);
	}
	
}

// osc::OutboundPacketStream& operator<<(osc::OutboundPacketStream& stream, const uint64_t& num)
// {
//     // write obj to stream
//     return os;
// }

void Chordata::Communicator::bundle_clear(){
			// COPP_Bundle_Manager& man = get_bundle_manager();
			// man.q_list.clear();
			// man.q_data = false;
			// man.raw_list.clear();
			// man.raw_data = false;
		}

void Chordata::Communicator::OscOut::transmit_q_bundle(int num_msgs){

	static string q_bundle_header = "/%%" _CHORDATA_DEF_OSC_ADDR "/q";

	COPP_Bundle_Manager& man = get_bundle_manager();
	if ( !Chordata::getConf().comm.use_bundles ){
		return;
	}

	uint64_t bundle_time;

	while (	man.q_list.size() > num_msgs){

		auto el =  man.q_list.front();
		bundle_time = el.time;
		p << osc::BeginBundle(bundle_time);
		// int counter = 0;

		p << osc::BeginMessage( q_bundle_header.c_str() )
		<< osc::OscNil << osc::EndMessage;

		while ( el.time == bundle_time){
			// const char* addr = el.addr.c_str();
			// if (addr == nullptr) {
			// 	return;
			// } 

			p << osc::BeginMessage( el.addr.c_str() )
			<< el.q.w << el.q.x << el.q.y << el.q.z << osc::EndMessage;
			// counter ++;

			man.q_list.pop();

			if (man.q_list.empty()) break;
			el =  man.q_list.front();
		}
	    p << osc::EndBundle;
	    flush_();

	}
}

void Chordata::Communicator::OscOut::transmit_raw_bundle(int num_msgs){
	static string raw_bundle_header = "/%%" _CHORDATA_DEF_OSC_ADDR "/raw";

	COPP_Bundle_Manager& man = get_bundle_manager();
	if ( !Chordata::getConf().comm.use_bundles ){
		return;
	}

	uint64_t bundle_time;
	while (	man.raw_list.size() > num_msgs){

		auto el =  man.raw_list.front();
		bundle_time = el.time;
		p << osc::BeginBundle(bundle_time);
		int counter = 0;

		p << osc::BeginMessage( raw_bundle_header.c_str() )
		<< osc::OscNil << osc::EndMessage;

		while ( el.time == bundle_time){
			// const char* addr = el.addr.c_str();
			// if (addr == nullptr) {
			// 	return;
			// } 
			// 
			p << osc::BeginMessage( el.addr.c_str() )
			<< el.gx << el.gy << el.gz
			<< el.ax << el.ay << el.az
			<< el.mx << el.my << el.mz
			<< osc::EndMessage;

			counter ++;

			man.raw_list.pop();

			if (man.raw_list.empty()) break;
			el =  man.raw_list.front();
		}
	    
	    p << osc::EndBundle;
	    flush_();

	}
}

void Chordata::Communicator::OscOut::transmit_pointc_bundle(int num_msgs){
	static string pointc_bundle_header = "/%%" _CHORDATA_DEF_OSC_ADDR "/extra";

	COPP_Bundle_Manager& man = get_bundle_manager();
	if ( !Chordata::getConf().comm.use_bundles ){
		return;
	}

	uint64_t bundle_time;
	int bundles;

	while (	man.pointc_list.size() > num_msgs ){

		auto el =  man.pointc_list.front();
		bundle_time = el.time;
		p << osc::BeginBundle(bundle_time);
		// int counter = 0;

		p << osc::BeginMessage( pointc_bundle_header.c_str() )
		<< osc::OscNil << osc::EndMessage;

		while ( el.time == bundle_time){
			// const char* addr = el.addr.c_str();
			// if (addr == nullptr) {
			// 	return;
			// } 

			p << osc::BeginMessage( el.addr.c_str() )
			<< el.mx << el.my << el.mz
			<< osc::EndMessage;

			man.pointc_list.pop();

			if (man.pointc_list.empty()) break;
			el =  man.pointc_list.front();
		}
		bundles ++;
	    p << osc::EndBundle;
	    flush_();

	}
	Chordata::Communicator::debug("Point cloud bundles sent: {} ", bundles);

}


void Chordata::Communicator::OscOut::transmit(const std::string& s, const Quaternion& q){
	p << osc::BeginMessage( s.c_str() )
    // << static_cast<int64_t>(timekeeper.ellapsedMicros()) 
    << q.w << q.x << q.y << q.z << osc::EndMessage;

    flush_();
}

void Chordata::Communicator::OscOut::transmit(const std::string& s, int16_t gx, int16_t gy, int16_t gz,
		        					int16_t ax, int16_t ay, int16_t az,
		        					int16_t mx, int16_t my, int16_t mz){
	p << osc::BeginMessage( s.c_str() )
    // << static_cast<int64_t>(timekeeper.ellapsedMicros()) 
    << gx << gy << gz
    << ax << ay << az
    << mx << my << mz
    << osc::EndMessage;

    flush_();
}

void Chordata::Communicator::OscOut::transmit(const std::string& s, float a, float b){
	p << osc::BeginMessage( s.c_str() )
    << a << b << osc::EndMessage;
    flush_();
}

void Chordata::Communicator::transmit(const std::string& s, int16_t gx, int16_t gy, int16_t gz,
		        					int16_t ax, int16_t ay, int16_t az,
		        					int16_t mx, int16_t my, int16_t mz){
	if (osc_sink)
		osc_sink->transmit(s, gx, gy, gz, ax, ay, az, mx, my, mz);

}

void Chordata::Communicator::transmit(const std::string& s, float a, float b){
	if (osc_sink)
		osc_sink->transmit(s, a, b);

}

void Chordata::Communicator::transmit_bundles(int num_msgs){
	if (!osc_sink)
		return;
	// auto current_time = timekeeper.ellapsedMicros();
	osc_sink->transmit_q_bundle(num_msgs);

	osc_sink->transmit_raw_bundle(num_msgs);
}

void Chordata::Communicator::transmit_pointc(){
	if (!osc_sink)
		return;

	osc_sink->transmit_pointc_bundle(global_odr_modifiers);

}

// void Chordata::Communicator::transmit(const Node *n, int16_t gx, int16_t gy, int16_t gz,
// 		        					int16_t ax, int16_t ay, int16_t az,
// 		        					int16_t mx, int16_t my, int16_t mz){
	
// 	Chordata::Communicator::transmit(n->raw_osc_addr, gx, gy, gz, ax, ay, az, mx, my, mz);

// }

void Chordata::Communicator::OscOut::sink_it_(const spdlog::details::log_msg& msg){
	p << osc::BeginMessage( (msg.level < spdlog::level::warn)? OscOut::comm_address.c_str() : OscOut::error_address.c_str() )
    << fmt::to_string(msg.raw).c_str() << osc::EndMessage;

    flush_();
}

void Chordata::Communicator::OscOut::flush_(){
	transmitSocket.Send( p.Data(), p.Size() );
	p.Clear();
}

#define print_with_millis(stream, text) \
	stream << "["; \
	stream.width( _CHORDATA_PRINT_MILLIS_PADDING ); \
	stream << timekeeper.ellapsedMillis(); \
	stream << "] " << text << std::endl;
