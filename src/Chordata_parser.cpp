/**
 * @file Chordata_parser.cpp
 * Classes here allow parsing of the command line and xml files
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include <iostream>
#include <string>
#include <regex>
#include <limits>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <array>

#include "tinyxml2/tinyxml2.h"
#include "fmt/core.h"
#include "fmt/format.h"
#include "pstreams/pstream.h" // to get the output of a system call

#include "Chordata_parser.h"
#include "Chordata_utils.h"
#include "Chordata_communicator.h"
#include "Chordata_imu.h"


using namespace Chordata;
using namespace std;
using namespace tinyxml2;
using fmt::format;
namespace comm = Chordata::Communicator;

namespace Chordata{
	// typedef std::function<const Chordata::Configuration_Data&(void)> config_fn;

	// extern config_fn getConf;
}


void Cmd_Line_Validator_OutputFlag::operator()(const std::string& name, const std::string& value, Chordata::Output_Redirect& destination){
    	if (first_time_here) {
    		destination = Chordata::NONE;
    		first_time_here =  false;
    	}

    	for (char i = 0; i < _Output_Redirect_keywords_n; ++i)
    	{
    		if(value.compare(_Output_Redirect_keywords[i]) == 0){
    			destination |= static_cast<Output_Redirect>(1<<(i-1));
    			return;
    		}
    	}

    	throw args::ParseError(
    		format("The option passed to [{}] wasn't correct,\n Should be: {}", 
    		name, _Output_Redirect_REGEX ) 
    		.c_str()
    		);
    	
    }

Configuration_Data Chordata::createDefaultConfig(){
	const char *args[] = {"Fake_notochord"};
    return Chordata::parse_cmd_line(1, args );
}

Configuration_Data Chordata::createConfig_with_filenames(
			const std::string& file,
			const std::string& schema){
	Chordata::Configuration_Data config = createDefaultConfig();
	config.xml.filename = file;
	config.xml.schema = schema;
	return config;
}


Configuration_Data Chordata::parse_cmd_line(int argc, const char **argv){
	typedef args::ValueFlag<std::string> textFlag;
	typedef args::ValueFlag<Output_Redirect, Cmd_Line_Validator_OutputFlag> redirectFlag;

	args::ArgumentParser parser(_NOTOCHORD_DESCRIPTION, msgs::note );
	
	args::Positional<std::string> ip(parser, "ip", msgs::ip);
	args::Positional<int> port(parser, "port", msgs::port);
	
	args::HelpFlag help(parser, "help", msgs::help, {'h', "help"});
	args::Flag version(parser, "version", msgs::version, {'V', "version"});
	args::Flag credits(parser, "credits", "Information about people contributing to this program, libraries, etc", {"credits"});
	args::Flag wait(parser, "no_wait", msgs::wait, {'y', "yes"});
	args::Flag raw(parser, "raw_lectures", msgs::raw, {'r', "raw"});

	args::Flag madgwick(parser, "Madgwick's sensor fusion", "Use Madgwick's sensor fusion algorithm (legacy)", {"madgwick"});
	args::Flag enable_mag_correction(parser, "live-mag-correction", "Enable live magnetic correction in Kalman filter", {"live-mag-correction"});
	

	args::Flag scan(parser, "scan Armature", msgs::scan, {"scan"});
	args::Flag calib(parser, "Calibrate", msgs::calib, {'x', "calibrate", "calib"});

	args::ValueFlag<int> kc_ver(parser, "K_Ceptor revision", msgs::kc_ver, {'k', "kceptor"});
	
	args::ValueFlag<int> odr(parser, "Sensor Output Data Rate", 
			"Read sensors at this frequency (default=50Hz, max=100Hz)", {"odr"});

    args::Group xml(parser, msgs::xml_g, args::Group::Validators::DontCare);

	textFlag xml_filename(xml, "xml_filename", msgs::xml_filename, {'c', "config"});
	textFlag xml_schema(xml, "xml_schema", msgs::xml_schema, {"schema"});

    args::Group comm_g(parser, msgs::comm_g, args::Group::Validators::DontCare);

	redirectFlag log(comm_g, "output", msgs::log, {'l', "log"});
	redirectFlag error(comm_g, "output", msgs::error, {'e', "errors"});
	redirectFlag transmit(comm_g, "output", msgs::transmit, {'t', "transmit"});
	// args::Flag runner(comm_g, "runner-mode", msgs::runner, {"runner_mode"});
	args::Flag no_bundles(comm_g, "no-bundles", "Don't use COPP bundles (send plain OSC messages)", {"no-bundles"});

	textFlag adapter(comm_g, "i2c_adapter", msgs::i2c, {'a', "i2c_adapter"});
	textFlag filename(comm_g, "log_filename", msgs::filename, {'f', "log_file"});

	//TODO: implement named level, using :
	// (spdlog/common.h:91) SPDLOG_LEVEL_NAMES { "trace", "debug", "info",  "warning", "error", "critical", "off" };
	args::ValueFlag<int> verbose(comm_g, "verbose_output", msgs::verbose, {'v', "verbose"});

	// args::ValueFlag<float> send_rate(comm_g, "send_rate", msgs::send_rate, {'s', "send-rate"});


	args::Group osc_g(parser, msgs::osc_g, args::Group::Validators::DontCare);

	textFlag osc_base(osc_g, "osc_addr", msgs::osc_base, {'b', "base_osc"});
	textFlag osc_err(osc_g, "osc_addr", msgs::osc_err, {"error_osc"});
	textFlag osc_comm(osc_g, "osc_addr", msgs::osc_comm, {"comm_osc"});

	// args::Group misc_g(parser, "miscelaneous", args::Group::Validators::DontCare);
	// args::ValueFlag<float>
	// 	beta(misc_g, "Beta", "Madgwick's sensor fusion Beta parameter", {'B', "Beta"});

	Configuration_Data conf;

	try{
      parser.ParseCLI(argc, argv);
   	
   	} catch (args::Help& h){   		
		throw CMDLine_Parse_Error(parser.Help(), h);

	} catch (args::Error& h){		
		throw CMDLine_Parse_Error(parser.Help(), h);

	}

	if (credits){
		auto h = args::Help("");
		throw CMDLine_Parse_Error("##credits##", h);		
	}

	using args::get;
	if (version){
		args::Error e("");
		throw CMDLine_Parse_Error(format(msgs::version_dump, _CHORDATA_VERSION_STR), e);
	}

	conf.wait = (wait)? false : true;
	conf.raw = (raw)? true : false;
	conf.calib = (calib)? true : false;
	conf.scan = (scan)? true : false;
	conf.mag_correction = (enable_mag_correction)? true : false;

	if (odr){
		if (get(odr) > 100){
			throw args::ParseError(
    		format("The --odr argument should be less or equal to 100, was {} ", get(odr)));
		} 
		conf.odr = get(odr);

	}

	if (kc_ver){
		conf.kc_ver = get(kc_ver);
	}


	if (xml_filename) conf.xml.filename = get(xml_filename);
	if (xml_schema) conf.xml.schema = get(xml_schema);

	if (ip) conf.comm.ip = get(ip);
	if (port) conf.comm.port = get(port);
	if (adapter) conf.comm.adapter = get(adapter);
	if (filename) conf.comm.filename = get(filename);
	if (verbose) conf.comm.log_level = std::min(get(verbose),2);
	if (log) conf.comm.log = get(log);
	if (error) conf.comm.error = get(error);
	if (transmit) conf.comm.transmit = get(transmit);

	if (osc_base) conf.osc.base_address = get(osc_base);
	if (osc_err) conf.osc.error_address = get(osc_err);
	if (osc_comm) conf.osc.comm_address = get(osc_comm);

	conf.comm.use_bundles = (no_bundles)? false : true;

	conf.fusion.madgwick = (madgwick)? true : false;

	conf.append_path();

	return conf;
}	//end parse_cmd_line()


bool XML_Parser::validate_xml(){
	//TODO: use a xml parsing library with support for xsd validation
	//..in the meanwhile we are using the cli tool "xmllint"

	int validation_result = -1;
	std::string line;
	fmt::memory_buffer buf;

	std::string system_call = format("xmllint --schema {} {}", 
								validation_filename, 
								filename);

	redi::ipstream proc(system_call.c_str(), 
		redi::pstreams::pstdout | redi::pstreams::pstderr);
	
	// xmllint puts a message with the result on stderr
	while (std::getline(proc.err(), line))
		format_to(buf, line + '\n');
		// buf.write( line + '\n' );

	validation_msg =  fmt::to_string(buf);

	proc.close();
		
	if (proc.rdbuf()->exited()){
		validation_result = proc.rdbuf()->status();
	}

	return (0 == validation_result)? true : false;

}

void XML_Parser::init_procedure(){
		if (!validate_xml()){
			throw XML_Parse_Error(
				format("Error on xml file validation:\n{}",validation_msg), 
				CONFIG_PARSING,  XML_SUCCESS);
		}

		lastError = LoadFile(filename.c_str());

		if (lastError != tinyxml2::XML_SUCCESS)
			throw XML_Parse_Error("Error on xml file parsing", CONFIG_PARSING,  lastError);
	
}

XML_Parser::XML_Parser(Configuration_Data & conf):
	XMLDocument(),
	filename(conf.xml.filename),
	validation_filename(conf.xml.schema), 
	baseNode(nullptr)
	{	
		//open and validate XML;
		init_procedure();

		const XMLElement *theNode;

		const XMLElement *conf_node = getBaseNode()
				->FirstChildElement("Configuration");
		
		const XMLElement *comm_node = conf_node
				->FirstChildElement("Communication");
		const XMLElement *osc_node = conf_node
				->FirstChildElement("Osc");
		const XMLElement *fusion_node = conf_node
				->FirstChildElement("Fusion");

		auto get = [](const XMLElement* e, const char* t){
			return e->FirstChildElement(t);
		};

		//Get a temp default conf, 
		//in order to parse the xml only if the current value is the default
		Configuration_Data default_conf;
		
		auto putInConf = [&theNode, get](string& target, const char* name){
				if (auto childNode = get(theNode,name)){
				target = childNode->GetText();
				boost::trim(target);
			}
		};

		auto validate_putInConf = 
			[&theNode, get](Output_Redirect& target, const char* name)
		{
			if (auto childNode = get(theNode,name)){
				Cmd_Line_Validator_OutputFlag validator;
				std::smatch m;
				std::string s(childNode->GetText());
				std::regex pattern (_Output_Redirect_REGEX );

				while (std::regex_search(s,m, pattern)){
					for (unsigned i=0; i<m.size(); ++i) {
						validator(name, m[i], target);
						s = m.suffix();
					}
				}
			}
		};
		
		//parse <Configuration> node
		theNode = getBaseNode()->FirstChildElement("Configuration");
		
		string temp;
		if (are_the_same(conf.kc_ver , default_conf.kc_ver)){
			putInConf(temp, "KC_revision"); 
			conf.kc_ver = stoi(temp);
		} else if (conf.kc_ver != 1 && conf.kc_ver != 2) {
			conf.kc_ver = _KC_REV;
		}

		//parse <Communication> node
		theNode = comm_node;
		if (are_the_same(conf.comm.adapter , default_conf.comm.adapter))
			 putInConf(conf.comm.adapter, "Adapter"); //** conf.comm.adapter **

		if (are_the_same(conf.comm.ip , default_conf.comm.ip))
			 putInConf(conf.comm.ip, "Ip"); //** conf.comm.ip **

		if (are_the_same(conf.comm.filename , default_conf.comm.filename))
			 putInConf(conf.comm.filename, "Filename"); //** conf.comm.filename **

		temp;
		if (are_the_same(conf.comm.port , default_conf.comm.port)){
			putInConf(temp, "Port"); //** conf.comm.port **
			conf.comm.port = stoi(temp);
		}

		if (are_the_same(conf.comm.log_level , default_conf.comm.log_level)){
			putInConf(temp, "Verbosity"); //** conf.comm.log_level **
			conf.comm.log_level = stoi(temp);
		}

		if (are_the_same(conf.comm.send_rate , default_conf.comm.send_rate)){
			putInConf(temp, "Send_rate"); //** conf.comm.log_level **
			conf.comm.send_rate = std::max(stof(temp),0.0f);
		}

		if (are_the_same(conf.comm.log , default_conf.comm.log))
			 validate_putInConf(conf.comm.log, "Log"); //** conf.comm.log **

		if (are_the_same(conf.comm.error , default_conf.comm.error))
			 validate_putInConf(conf.comm.error, "Error"); //** conf.comm.error **

		if (are_the_same(conf.comm.transmit , default_conf.comm.transmit))
			 validate_putInConf(conf.comm.transmit, "Transmit"); //** conf.comm.transmit **
	

		//parse <Osc> node
		theNode = osc_node;
		if (are_the_same(conf.osc.base_address , default_conf.osc.base_address))
			 putInConf(conf.osc.base_address, "Base"); //** conf.osc.base_address **

		if (are_the_same(conf.osc.error_address , default_conf.osc.error_address))
			 putInConf(conf.osc.error_address, "Error"); //** conf.osc.error_address **

		if (are_the_same(conf.osc.comm_address , default_conf.osc.comm_address))
			 putInConf(conf.osc.comm_address, "Info"); //** conf.osc.comm_address **
	

		//parse <Fusion> node
		theNode = fusion_node;
		if (are_the_same(conf.fusion.beta_s , default_conf.fusion.beta_s)){
			putInConf(temp, "Beta_start"); //** conf.fusion.beta_s **
			conf.fusion.beta_s = stof(temp);
		}

		if (are_the_same(conf.fusion.beta_f , default_conf.fusion.beta_f)){
			putInConf(temp, "Beta_final"); //** conf.fusion.beta_f **
			conf.fusion.beta_f = stof(temp);
		}

		
		if (are_the_same(conf.fusion.time , default_conf.fusion.time)){
			putInConf(temp, "Time"); //** conf.fusion.time **
			conf.fusion.time = stoi(temp);

		}

		if (conf.fusion.time > std::numeric_limits<float>::max()){
			throw Chordata::Error("Overflow error on beta time argument");
		}
	};


XMLNode *XML_Parser::getBaseNode(){

	if (baseNode == nullptr){
		baseNode = FirstChildElement();
		
		if (baseNode == nullptr)
			throw XML_Parse_Error("Error reading XML file", CONFIG_PARSING, XML_ERROR_FILE_READ_ERROR);
		
		if (string( _CHORDATA_XML_BASE ).compare(baseNode -> Value()) != 0  ) 
			throw XML_Parse_Error("\"<" _CHORDATA_XML_BASE ">\" Base node not found", CONFIG_PARSING, XML_SUCCESS);
		
		} 
	return baseNode;
}

string XML_Parser::getVersion(){
	const char* v = getBaseNode()->ToElement()->Attribute("version");
	if (v == nullptr)
		throw XML_Parse_Error("No version found in Chordata config file", CONFIG_PARSING,  XML_SUCCESS);
		
	return string(v);
}

Armature_Parser::Armature_Parser
			(Node_Scheluder *n,	Configuration_Data &conf):
			XML_Parser(conf),
			i2c(nullptr),
			scheluder(n),
			armatureNode(nullptr)
			{
				K_Ceptor::set_osc_base_addr(conf.osc.base_address);
			};


XMLNode *Armature_Parser::getArmatureNode(){
	
	if (armatureNode == nullptr){
		armatureNode = getBaseNode()-> FirstChildElement("Armature");
		if (armatureNode == nullptr ) 
			throw ARMATURE_XML_ERROR("No Armature information found", XML_SUCCESS);
	
		if (armatureNode -> NextSiblingElement("Armature") != nullptr) 
			throw ARMATURE_XML_ERROR("Found duplicated Armature information", XML_SUCCESS);
	}

	return armatureNode;
}

Armature_Parser::armature_ptr Armature_Parser::getArmature(I2C_io *_i2c){
	set_io(_i2c);

	comm::try_info("Starting Armature xml parsing.");
	return std::unique_ptr<Armature>( 
		new Armature(
				parseNode(
				getArmatureNode()->ToElement()-> FirstChildElement(),
				nullptr)
		)
	);
}

#define LSM9DS1_AG_ADDR 0x6B

Link* Armature_Parser::scanKCeptors(Link* first, std::string branch){
	Link* current = first;
	for (int trans_val = 0; trans_val < 16; ++trans_val){
		try{
			int counter = 0;
			while ( i2c->readByte(LSM9DS1_AG_ADDR ^ trans_val, WHO_AM_I_XG != WHO_AM_I_AG_RSP )){
				if (counter > 20) throw Chordata::IO_Error();
				counter ++;
				comm::try_warn("K_Ceptor found at {}, retrying WHO_AM_I query", trans_val);
			}

			auto label = fmt::format("kc-{}-{}", branch, trans_val );
			auto the_kc = new Chordata::K_Ceptor(current, trans_val, label, i2c);
			comm::try_info("Scan: ~~K-Ceptor [{}], value {}", label, trans_val);

			if (current)
				current->addChild( the_kc );
			
			scheluder->addNode( the_kc );
			current = static_cast<Link*>(the_kc);

			if (!first)
				first = current;

			if (Chordata::getConf().calib) return first;

			
		} catch  (const Chordata::IO_Error& e) {
			continue;
		}
	}
	return first;
}

Armature_Parser::armature_ptr Armature_Parser::scanArmature(I2C_io *_i2c){

	set_io(_i2c);

	comm::try_info("Starting Armature scanning.");

	Link* first = nullptr;

	try{ //to find a Hub
		_i2c->readByte(_CHORDATA_MUX_ADDR, 0);
		auto the_mux = new Chordata::Mux(nullptr, _CHORDATA_MUX_ADDR, "Base Mux", _i2c);
		first = static_cast<Link*>(the_mux);
		comm::try_info("Scan: [Base Mux] Hub");
	} catch  (const Chordata::IO_Error& e) {
		comm::try_info("Scan: No Hub found");
	}

	if (first) { //There's a hub, open the gates one at a time and scan for KCs
		for (int CH = 1; CH <= 6; ++CH){
			auto branch_label = fmt::format("CH_{}", CH);
			auto branch_ch = static_cast<mux_channel>(1 << (CH-1));
			auto parent_mux = dynamic_cast<Chordata::Mux*>(first);
			auto the_branch = 
				new Chordata::Branch(parent_mux, _CHORDATA_MUX_ADDR, branch_label, branch_ch, _i2c);
			_i2c->writeByte(_CHORDATA_MUX_ADDR, 0, (1 << (CH-1)));
			comm::try_info("Scan: [Base Mux] Hub, GATE: {}", branch_label);
			scanKCeptors(the_branch, branch_label);

			if (the_branch->getChild()){
				scheluder->addNode( the_branch );
				if (Chordata::getConf().calib) break;
			}
		}
		
	} else { //No Hub, just scan for KCs
		first =	scanKCeptors(first, "none");

	}

	if (!first) return nullptr;
	return std::unique_ptr<Armature>(new Armature( first ));
}

#define complain(PATTERN) throw Chordata::Logic_Error(fmt::format(PATTERN, \
			content, xml_node->Value(), \
			((xml_node->Attribute("Name"))? xml_node->Attribute("Name") : "")\
				)\
			);

uint8_t Armature_Parser::getAddress(tinyxml2::XMLElement *xml_node){
	using boost::trim;
	string content(xml_node->GetText());
	trim(content);
		
	try	{
		uint8_t result = std::stoi(content, nullptr, 0);
		// for (int i = 0; i < invalid_i2c_addr_n; ++i){
		// 	if (invalid_i2c_addr[i] == result){
		// 		complain("The address [{}] in the node <{}>{} is not valid");
		// 	}
		// }
		if (result > 127) complain("The address [{}] in the node <{}>{} is not valid");

		comm::try_debug(" | address: 0x{:x}", result);

		return result;

	} catch (const std::invalid_argument&){
		// throw Chordata::Idiot_Error();
		complain("The address [{}] in the node <{}>{} is not valid");
		
	} catch (const std::out_of_range&){
		// throw Chordata::Idiot_Error();
		complain("The address [{}] in the node <{}>{} is out of range");
	}
}

Armature_Parser::node_type Armature_Parser::getNodeType(XMLElement *xml_node){
	//types must match Armature_Parser::node_type
	static const string types[] = {
		"Null", "Mux", "Branch", "K_Ceptor"
	};

	for (int i = 0; i < 4; ++i)
	{ 
		if (types[i].compare(xml_node->Value()) == 0) 
			return static_cast<Armature_Parser::node_type>(i); 
	}

	return static_cast<Armature_Parser::node_type>(0);
}

mux_channel Armature_Parser::getBranchCH(XMLElement *xml_node){
	using boost::trim;
	//channels must match enum mux_channel (Chordata_def.h)
	static const string channels[] = {
		"OFF", "CH_1", "CH_2", "CH_3", "CH_4", "CH_5", "CH_6" 
	};

	string content(xml_node->GetText());
	trim(content);

	for (int i = 0; i < 7; ++i)
	{ 
		if (content.compare(channels[i]) == 0) {
			comm::try_debug(" | channel : {} ", content);
			return static_cast<mux_channel>(1 << (i-1));
		} 
	}

	comm::try_warn("Branch ({}) channel not recognised: {}", 
			((xml_node->Attribute("Name"))? xml_node->Attribute("Name") : ""),
			content);

	return static_cast<mux_channel>(0);
}

float Armature_Parser::min_float_value = std::numeric_limits<float>::min();

Armature_Parser::offset 
Armature_Parser::getCalibration_offset(tinyxml2::XMLElement * this_node, const std::string& target){
 	XMLElement *calibration = this_node ->FirstChildElement("calibration");

 	while (calibration){
	 	if (string(calibration->Attribute("type")).compare("offset") == 0){	
	 		if (string(calibration->Attribute("target")).compare(target) == 0){
				string calibs[3];
			 	splitMax(string(calibration->GetText()),calibs,3);
			 	
			 	return std::make_tuple((float) atof(calibs[0].c_str()),
			 						(float) atof(calibs[1].c_str()),
			 						(float) atof(calibs[2].c_str()));
	 		}
	 	}
	 	calibration = calibration->NextSiblingElement("calibration");
 	}

 	return std::make_tuple(min_float_value, min_float_value, min_float_value);
}

Armature_Parser::matrix3x3 
Armature_Parser::getCalibration_matrix(tinyxml2::XMLElement * this_node, const std::string& target){
 	XMLElement *calibration = this_node ->FirstChildElement("calibration");

 	while (calibration){
	 	if (string(calibration->Attribute("type")).compare("matrix") == 0){	
	 		if (string(calibration->Attribute("target")).compare(target) == 0){
				string calibs[9];
			 	splitMax(string(calibration->GetText()),calibs,9);
			 	
			 	matrix3x3 result;

			 	for (int col = 0; col < 3; ++col)	{
			 		for (int row = 0; row < 3; ++row){
			 			result[row][col] = (float) atof(calibs[row*3 +col].c_str());
			 		}
			 	}

			 	return result;
	 		}
	 	}
	 	calibration = calibration->NextSiblingElement("calibration");
 	}

 	return { {{0,0,0}, {0,0,0}, {0,0,0}} };
}


Link *Armature_Parser::parseNode(	XMLElement *this_node, 
									Link *parent_node,
									int depth ){
    if (this_node == nullptr) {
        return nullptr;
    }

    auto getChildNode = [this_node]()-> decltype(this_node) {
    	static const char* names[] = {"Mux", "Branch", "K_Ceptor"};
    	for (auto n:names){
    		auto child_node = this_node -> FirstChildElement(n);
    		if (child_node != nullptr) return child_node;
    	}
    	return nullptr;
    };

	Chordata::Communicator::try_debug("{}<{}> {}", 
		string(depth*2, ' '), 
		this_node->Value(),
		((this_node->Attribute("Name"))? this_node->Attribute("Name") : "")
		);


	Chordata::Mux *parent_mux = dynamic_cast<Chordata::Mux*>(parent_node);
    
    switch ( getNodeType(this_node) ){
    	
    	case Branch:
			{	//Only one child, include in scheluder
				if ( parent_mux == nullptr){
					throw Hierarchy_Error(fmt::format("This Branch node <{}> doesn't have a Mux parent", this_node -> Attribute("Name")));
				}

		 		auto this_link = make<Chordata::Branch>(parent_mux, this_node);
 	
 		 		auto child_node = getChildNode();

		 		this_link->addChild( parseNode(child_node, this_link, ++depth ) );
		 		depth--;
		 		scheluder->addNode(this_link);
		 		return static_cast<Link*>(this_link);

		 	}
    	case K_Ceptor:
 			{	//Only one child, include in scheluder
 				if ( parent_mux  != nullptr){
					throw Hierarchy_Error(fmt::format("This K_Ceptor node <{}> has a Mux parent", this_node -> Attribute("Name")));
				}
 		 		
		 		auto this_link = make<Chordata::KC>(parent_node, this_node);

		 		Armature_Parser::offset null_offset = 
		 				std::make_tuple(min_float_value, min_float_value, min_float_value);
		 		Armature_Parser::matrix3x3 null_matrix = { {{0,0,0}, {0,0,0}, {0,0,0}} };

				auto offset = getCalibration_offset(this_node, "mag");
				if (offset != null_offset)
				 	this_link->setMagOffset(offset);

				auto matrix = getCalibration_matrix(this_node, "mag");
				if (matrix != null_matrix)
					this_link->setMagMatrix(matrix); 

			 	offset = getCalibration_offset(this_node, "gyro");
				if (offset != null_offset)
			 	 	this_link->setGyroOffset(offset);

			 	offset = getCalibration_offset(this_node, "acel");
				if (offset != null_offset)
			 	 	this_link->setAcelOffset(offset);

			 	this_link->setBeta(Chordata::getConf().fusion.beta_s); 
 		 		auto child_node = getChildNode();

 		 		this_link->addChild( parseNode(child_node, this_link, ++depth ) );
 		 		depth--;
 		 		scheluder->addNode(this_link);
 		 		return static_cast<Link*>( this_link);
 		 	}
    	
		case Mux:
    		{	//Multiple Childs, dont include in scheluder
    			if ( parent_mux  != nullptr){
					throw Hierarchy_Error(fmt::format("This Mux node <{}> has a Mux parent", this_node -> Attribute("Name")));
				}

		 		auto this_link = make<Chordata::Mux>(parent_node, this_node);

    	    	auto child_node = getChildNode();
    	    	
    	    	while (child_node != nullptr) {
    	    		try{
    	    			this_link->addChild( parseNode(child_node, this_link, ++depth ) );
    	    			
    	    		} catch ( const Hierarchy_Error&){
    	    			throw Hierarchy_Error(format("Exceded the maximum number of children ({}) on the Mux: {}",
    	    				this_link->getMax(),
    	    				this_node->Attribute("Name")
    	    				)
    	    			);
    	    		}
    	    		child_node = child_node->NextSiblingElement();
    	    		depth--;
    	    	}
    	    	return static_cast<Link*>( this_link);
    	    }

    	default:
    	return nullptr;

    }
}

