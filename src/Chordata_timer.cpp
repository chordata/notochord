/**
 * @file Chordata_timer.cpp
 * Time-handlind classes: Scheduler to organize the reading of the nodes, and Timer to launch each read. 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "Chordata_communicator.h"
#include "Chordata_timer.h"
#include "Chordata_def.h"
#include "Chordata_utils.h"
#include "Chordata_node.h"
#include <iostream>
#include <cmath>


namespace comm = Chordata::Communicator;

using namespace std;
using namespace Chordata;
using scheduler_citer = std::vector<I2C_Node*>::const_iterator;

#define LAP_MODIF_FACTOR 400
#define LAP_TOLERANCE 100
#define LAP_CORRECT_TIME_MARGIN 10*1000

void Node_Scheluder::addNode(I2C_Node *n){
		nodelist.push_back(n);
		current = nodelist.end()-1;

		if (n->isSensor())
			ODRModifiers++;
		
	};

const scheduler_citer seek_first_sensor(const std::vector<I2C_Node*>& list){
	for (scheduler_citer i = list.begin(); i != list.end(); ++i){
		if ((*i)->isSensor()) {
			(*i)->first_sensor = true;
			comm::info("First sensor in Scheduler: {}", (*i)->getLabel());
			return i;
		}
	}
}

uint64_t Chordata::global_lap_micros;
uint64_t Chordata::global_round_micros;


long int Node_Scheluder::bang(long int wait){
	//not threaded at the moment
	// thread nodeSearch(&Node_Scheluder::doSearch, this);
	// nodeSearch.detach();
	
	static long int lap_theoric_duration = 1000000 / Chordata::getConf().odr;
	static auto first_sensor = seek_first_sensor(nodelist);
	static int counter = Chordata::getConf().odr;
	
	// Chordata::K_Ceptor* k  = dynamic_cast< Chordata::K_Ceptor*>(*current);
	// if (k) comm::info("CURRENT: {}", k->getLabel());
	doSearch();


	global_lap_micros  = comm::timekeeper.ellapsedLapMicros();
	if (current == first_sensor){
		global_round_micros  = comm::timekeeper.ellapsedMicros();
		comm::transmit_bundles(getODRModifiers());
		// comm::info("FIRST: {}", k->getLabel());

		//Check if global_lap_micros duration is correct and adjust;
		if (counter < 1){
			counter = Chordata::getConf().odr;
			comm::trace(" ===== Ellapsed Lap Micros: {} | Hz: {:.2f} =====", global_lap_micros, (float)1000000.0/global_lap_micros);
			for ( I2C_Node* n : nodelist) { 
				if ( Chordata::K_Ceptor* kc  = dynamic_cast< Chordata::K_Ceptor*>(n)){
					// comm::trace("Node {:>30} lap: {:10d} micros", kc->getLabel(), kc->ellapsedLapMicros());
					
					if (!Chordata::getConf().fusion.madgwick){
						comm::transmit(fmt::format("/Chordata/extra/{}/amcov",kc->getLabel() ), 
							kc->get_kalman().get_a_covariance(), 
							kc->get_kalman().get_m_covariance());

					}
				}

			}
			
			if (comm::timekeeper.ellapsedMillis() > LAP_CORRECT_TIME_MARGIN ){
				//Leave the first second of execution untouched
				//after that start cheking if the global_lap_micros has to be increased or decreased
				
				if (global_lap_micros > lap_theoric_duration + LAP_TOLERANCE){
					float mod_factor = std::fabs(LAP_MODIF_FACTOR * ((int64_t)global_lap_micros - lap_theoric_duration) / lap_theoric_duration);
					wait -= mod_factor;
					
					if (wait < 0) wait = 0;

					comm::trace(" (- ) Read wait reduced, now is {} (wait theoric = {}, [Lap measured: {}, Lap theoric: {}], factor: {})", 
						wait, theoric_wait,
						global_lap_micros, lap_theoric_duration, mod_factor);
				
				} else if (global_lap_micros < lap_theoric_duration - (LAP_TOLERANCE * 2)){
					float mod_factor = std::fabs(LAP_MODIF_FACTOR * ((int64_t)global_lap_micros - lap_theoric_duration) / lap_theoric_duration);
					wait += mod_factor;
					
					comm::trace(" ( +) Read wait increased, now is {} (wait theoric = {}, [Lap measured: {}, Lap theoric: {}], factor: {})", 
						wait, theoric_wait,
						global_lap_micros, lap_theoric_duration, mod_factor);
						
				}
			}
		}
		counter --;
		comm::timekeeper.resetLap();
		
	} // End global_lap_micros duration cheking
	

	return wait;
}			

void Node_Scheluder::doSearch(){
	//not threaded at the moment
	// mp->lock();
		// cout << "LABEL: " << (*current)->getLabel() << endl;
		main_buffer.put(*current);
	// mp->unlock();
	step();

	while (!(*current)->isSensor()){
		main_buffer.put(*current);
		step();
	}

	//TODO: at this point inform another thread, and it will "eat" the nodes in the buffer.
	//This other thread will be responsable for checking eventual throws of the banged node.
	main_buffer.eat();
	
}


////////////////////////////////// 
/// for testing individual utils
//////////////////////////////////

#ifdef __TEST_TIMER__
int main(int argc, char const *argv[])
{	
	typedef Timer<Node_Scheluder> Timer_Sch;
	
	Node_Scheluder n;
	mutex *m = n.getMutex();
	Timer_Sch t(&n, 50000, m);

	// thread contador(&Timer_Sch::startTimer, t );
	thread contador([&]{t.startTimer();} );
	

	// while(1){
	// 		m->lock();
	// 			cout<< "MAIN THREAD MUTEX: " << m << endl;
	// 		m->unlock();
	// 		thread_sleep(micros(1000000));
	// }

	if(contador.joinable())
		contador.join();

	return 0;
}

#endif
